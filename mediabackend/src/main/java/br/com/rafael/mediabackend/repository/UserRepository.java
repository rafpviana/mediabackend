package br.com.rafael.mediabackend.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.rafael.mediabackend.model.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {

    public User findByUsername(String username);
    
}
