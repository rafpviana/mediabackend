package br.com.rafael.mediabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MediaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediaBackendApplication.class, args);
	}

}
