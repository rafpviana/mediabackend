package br.com.rafael.mediabackend.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.rafael.mediabackend.model.entity.Media;
import br.com.rafael.mediabackend.repository.MediaRepository;
import br.com.rafael.mediabackend.util.ConvertMultipartFileToFile;

@Service
public class MediaService {

    private final String BASE_FILE_URL = "https://media-rafpvianabucket.s3-sa-east-1.amazonaws.com/";
    
    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private AmazonS3Service amazonS3Service;

    @Autowired
    private ConvertMultipartFileToFile convertMultipartFileToFile;

    public List<Media> getMedias(Boolean onlyNotDeleted){
        if(onlyNotDeleted){
            return getNotDeletedMedias();
        }
        else{
            return getAllMedias();
        }
    }

    private List<Media> getAllMedias(){
        Iterable<Media> mediasIterable = mediaRepository.findAll();
        List<Media> medias = new ArrayList<>();
        for(Media media : mediasIterable){
            medias.add(media);
        }
        return medias;
    }

    private List<Media> getNotDeletedMedias(){
        List<Media> medias = mediaRepository.findByDeleted(false);
        return medias;
    }

    public Media getMedia(Integer id) throws NoSuchElementException {
        try {
            return mediaRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("Mídia não encontrada");
        }
    }

    public Media createMedia(Media media, MultipartFile file) throws IOException {
        if(file != null){
            File mediaFile = convertMultipartFileToFile.convert(file);
            String url = "medias/"+UUID.randomUUID().toString();
            amazonS3Service.sendFileToAmazonS3(url, mediaFile);
            mediaFile.delete();
            String completeURL = BASE_FILE_URL + url;
            media.setUrl(completeURL);
        }
        media = mediaRepository.save(media);
        return media;
    }

    public Media updateMedia(Integer id, Media media) throws NoSuchElementException {
        try {
            Media oldMedia = mediaRepository.findById(id).get();
            oldMedia.setName(media.getName());
            oldMedia.setUrl(media.getUrl());
            oldMedia.setDuration(media.getDuration());
            oldMedia.setUploadDate(media.getUploadDate());
            oldMedia.setDeleted(media.getDeleted());
            oldMedia = mediaRepository.save(oldMedia);
            return oldMedia;
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("Mídia não encontrada");
        }
    }

    public void deleteMedia(Integer id) throws NoSuchElementException {
        try {
            Media media = mediaRepository.findById(id).get();
            media.setDeleted(true);
            mediaRepository.save(media);
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("Mídia não encontrada");
        }
    }

}
