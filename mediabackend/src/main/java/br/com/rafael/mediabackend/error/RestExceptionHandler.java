package br.com.rafael.mediabackend.error;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.servlet.ServletException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
            HttpHeaders httpHeaders, HttpStatus httpStatus, WebRequest webRequest) {
        List<ErrorObject> errorObjectList = exception.getBindingResult().getFieldErrors().stream()
            .map(error -> new ErrorObject(error.getDefaultMessage(), error.getField(), error.getRejectedValue()))
            .collect(Collectors.toList());
        ErrorResponse errorResponse = new ErrorResponse("A requisição possui campos inválidos", httpStatus.value()
            , httpStatus.getReasonPhrase(), exception.getBindingResult().getObjectName(), errorObjectList);
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    private ErrorResponse generateErrorResponseForSingleErrorObject(HttpStatus httpStatus, String errorResponseMessage
        , String errorObjectMessage){
        List<ErrorObject> errorObjectList = new ArrayList<>();
        errorObjectList.add(new ErrorObject(errorObjectMessage, null, null));
        ErrorResponse errorResponse = new ErrorResponse(errorResponseMessage
            , httpStatus != null ? httpStatus.value() : null
            , httpStatus != null ? httpStatus.getReasonPhrase() : null
            , null, errorObjectList);
        return errorResponse;
    }

    @ExceptionHandler({NoSuchElementException.class})
    private ResponseEntity<Object> handleNoSuchElement(NoSuchElementException exception){
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        ErrorResponse errorResponse = generateErrorResponseForSingleErrorObject(httpStatus, "O recurso não foi encontrado"
            , exception.getMessage());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    @ExceptionHandler({IOException.class})
    private ResponseEntity<Object> handleIO(IOException exception){
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        ErrorResponse errorResponse = generateErrorResponseForSingleErrorObject(httpStatus, "Erro interno do servidor"
            , exception.getMessage());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    @ExceptionHandler({ServletException.class})
    private ResponseEntity<Object> handleServlet(ServletException exception){
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse errorResponse = generateErrorResponseForSingleErrorObject(httpStatus, "Erro interno do servidor"
            , exception.getMessage());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

    @ExceptionHandler({BadCredentialsException.class})
    private ResponseEntity<Object> handleBadCredentials(BadCredentialsException exception){
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse errorResponse = generateErrorResponseForSingleErrorObject(httpStatus, "Erro interno do servidor"
            , exception.getMessage());
        return new ResponseEntity<>(errorResponse, httpStatus);
    }

}
