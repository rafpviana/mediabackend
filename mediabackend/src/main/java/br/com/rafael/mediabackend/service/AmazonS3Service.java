package br.com.rafael.mediabackend.service;

import java.io.File;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AmazonS3Service {

    @Value("${amazons3.accesskey}")
    private String accessKey;
    
    @Value("${amazons3.secretkey}")
    private String secretKey;
    
    @Value("${amazons3.bucketName}")
    private String bucketName;

    @Value("${amazons3.bucketRegion}")
    private String bucketRegion;
    
    public void sendFileToAmazonS3(String url, File file){
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 s3client = AmazonS3ClientBuilder
            .standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion(Regions.fromName(bucketRegion))
            .build();
        s3client.putObject(bucketName, url, file);
    }

}
