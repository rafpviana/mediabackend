package br.com.rafael.mediabackend.controller;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.rafael.mediabackend.model.entity.Media;
import br.com.rafael.mediabackend.service.MediaService;

@RestController
@RequestMapping("/medias")
public class MediaController {

    @Autowired
    private MediaService mediaService;

    @GetMapping
    public ResponseEntity<List<Media>> getMedias(
            @RequestParam(name = "onlyNotDeleted", required = false, defaultValue = "false") Boolean onlyNotDeleted) {
        List<Media> medias = mediaService.getMedias(onlyNotDeleted);
        return ResponseEntity.ok(medias);
    }

    @GetMapping("{id}")
    public ResponseEntity<Media> getMedia(@PathVariable("id") Integer id) throws NoSuchElementException {
        Media media = mediaService.getMedia(id);
        return ResponseEntity.ok(media);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Media> createMedia(@Valid @RequestPart("media") Media media, @RequestPart("file") MultipartFile file)
            throws IOException{
        media = mediaService.createMedia(media, file);
        return ResponseEntity.ok(media);
    }

    @PutMapping("{id}")
    public ResponseEntity<Media> updateMedia(@PathVariable("id") Integer id, @Valid @RequestBody Media media)
            throws NoSuchElementException {
        media = mediaService.updateMedia(id, media);
        return ResponseEntity.ok(media);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Object> deleteMedia(@PathVariable("id") Integer id) throws NoSuchElementException {
        mediaService.deleteMedia(id);
        return ResponseEntity.ok().build();
    }
    
}
