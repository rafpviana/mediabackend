package br.com.rafael.mediabackend.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Data
public class Media {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Min(0)
    @Max(10000000)
    private Integer id;
    
    @NotNull(message = "O campo nome não pode ser nulo")
    @Size(max = 512)
    private String name;

    @Size(max = 512)
    private String url;

    @Min(0)
    @Max(10000000)
    private Integer duration;

    @Column(name = "upload_date")
    @NotNull(message = "O campo data de upload não pode ser nulo")
    private Date uploadDate;

    @NotNull(message = "O campo deletado não pode ser nulo")
    private Boolean deleted;

}
