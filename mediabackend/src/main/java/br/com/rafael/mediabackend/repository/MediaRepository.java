package br.com.rafael.mediabackend.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.rafael.mediabackend.model.entity.Media;

public interface MediaRepository extends CrudRepository<Media, Integer> {

    public List<Media> findByDeleted(Boolean deleted);

}