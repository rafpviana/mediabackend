package br.com.rafael.mediabackend.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ConvertMultipartFileToFile {

    public File convert(MultipartFile multipartFile) throws IOException {
        try {
            File file = new File(multipartFile.getOriginalFilename());
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(multipartFile.getBytes());
            fileOutputStream.close();
            return file;
        } catch (IOException e) {
            throw new IOException("Falha ao criar o arquivo");
        }
    }
    
}
