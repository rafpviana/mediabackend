- Pré-requisitos:

    - Java 11
    - MySQL Server
    - AmazonS3 Bucket

- Como executar a aplicação via IDE:

    - Clone o projeto
    - Insira as credenciais do bucket AmazonS3 no arquivo "/mediabackend/src/main/resources/application.properties"
    - Insira as credenciais do MySQL no arquivo "/mediabackend/src/main/resources/application.properties"
    - Importe a pasta do projeto para a IDE de sua preferência
    - Execute a classe "/mediabackend/src/main/java/br/com/rafael/mediabackend/MediaBackendApplication.java"

- Como executar a aplicação via linha de comando:

    - Clone o projeto
    - Insira as credenciais do bucket Amazon S3 no arquivo "/mediabackend/src/main/resources/application.properties"
    - Insira as credenciais do MySQL no arquivo "/mediabackend/src/main/resources/application.properties"
    - Acesse a pasta "/mediabackend" via linha de comando
    - Execute o comando "mvn clean package"
    - Acesse a pasta "/mediabackend/target"
    - Execute o comando "java -jar mediabackend-0.0.1-SNAPSHOT.jar"

- Não foram implementados testes unitários para esta aplicação

- Dentro da pasta "helpfiles" existem alguns arquivos de documentação e ajuda:

    - O arquivo "media-backend-openapi3.json" contém a especificação OpenAPI da API
    - O arquivo "media-backend.postman_collection.json" contém a coleção de requisições do Postman para interação com a API
    - O arquivo "media.json" contém as informações (meta-dados) de uma mídia, que poderá ser utilizado no corpo da requisição de criação de uma mídia na API
    - O arquivo "file.avi" é o vídeo propriamente dito, que poderá ser utilizado no corpo da requisição de criação de uma mídia na API

- A aplicação está disponível na AWS por meio do link:

    - ec2-15-228-52-37.sa-east-1.compute.amazonaws.com:8080

- Para acessar os endpoints da API é necessário primeiro registrar um usuário por meio do caminho "/register", para que então possa se autenticar com este usuário e obter um token de acesso JWT por meio do caminho "/authenticate" (exemplos de ambas as requisições estão incluídas no arquivo de coleção do Postman)